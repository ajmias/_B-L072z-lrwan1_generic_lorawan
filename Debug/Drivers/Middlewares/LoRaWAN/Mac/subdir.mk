################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.c \
../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.c 

OBJS += \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.o \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.o 

C_DEPS += \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.d \
./Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMac.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacAdr.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacClassB.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCommands.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacConfirmQueue.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacCrypto.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacParser.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.o: ../Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DDEBUG -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Mac/LoRaMacSerializer.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

